const createList = (array, parent) => {
    const ul = document.createElement('ul');
    parent.appendChild(ul);
    array.forEach(function generateList(item) {
        const li = document.createElement('li');
        ul.appendChild(li);
        if(Array.isArray(item)){
            createList(item, li);
        } else {
            li.innerHTML = item;
        }
    });
};

const arr = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
const list = document.body;

createList(arr, list);
